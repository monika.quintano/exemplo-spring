package br.com.itau.marketplace.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.Cliente;
import br.com.itau.marketplace.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	ClienteService clienteService;
	
	@GetMapping
	public Iterable<Cliente> listar() {
		return clienteService.obterClientes();
	}
	
	@GetMapping("/{id}")
	public Optional<Cliente> listarPorId(@PathVariable int id) {
		return clienteService.buscarClientePorId(id);
	}
	
	@PostMapping
	public void mostrarTchau(@RequestBody Cliente cliente) {
		clienteService.inserirCliente(cliente);
	}
}
